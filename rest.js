var mongodb = require('mongodb');
var mongo = mongodb.MongoClient;
var ObjectId = mongodb.ObjectId;
var WebSocket = require('ws');

var config = require("./config.js");
var lib = require('./lib');

var convertIdstoObjectIds = function(data) {
    for(var key in data) {
        if(key.endsWith("_id") && typeof data[key] == "string") {
            try {
                data[key] = ObjectId(data[key]);
            } catch(ex) {}
        } else if(key.endsWith("_ids") && Array.isArray(data[key])) {
            data[key].forEach(function(elem, index, arr) {
                try {
                    arr[index] = ObjectId(elem);
                } catch(ex) {}
            });
        }
    }
}

var rest = module.exports = {

    sessions: {},

    persons: null,
    groups: null,
    messages: null,

    login: function (req, rep, session) {
        switch(req.method) {
            case 'GET':
                lib.sendJson(rep, { session: session, user: rest.sessions[session].user, user_id: rest.sessions[session].user_id,
                    firstName: rest.sessions[session].firstName, lastName: rest.sessions[session].lastName, role: rest.sessions[session].role });
                break;
            case 'DELETE':
                if(rest.sessions[session]) {
                    rest.sessions[session].user = null;
                    rest.sessions[session].user_id = null;
                    rest.sessions[session].firstName = null;
                    rest.sessions[session].lastName = null;
                    rest.sessions[session].role = 0;
                    lib.sendJson(rep, { session: session, user: rest.sessions[session].user, user_id: rest.sessions[session].user_id,
                        firstName: rest.sessions[session].firstName, lastName: rest.sessions[session].lastName, role: rest.sessions[session].role });
                } else {
                    lib.sendXHRError(rep, 400, 'Logout failed');
                }
                break;
            case 'POST':
                lib.getPayload(req, function(req, err, data) {
                    if (err) {
                        lib.sendXHRError(rep, 400, err);
                    } else {
                        if(data["username"]) {
                            rest.persons.findOne({ login: data.username, password: data.password }, function(err, doc) {
                                if(err || !doc) {
                                    lib.sendXHRError(rep, 401, 'Login failed');
                                } else {
                                    rest.sessions[session].user = doc.login;
                                    rest.sessions[session].user_id = doc._id;
                                    rest.sessions[session].firstName = doc.firstName;
                                    rest.sessions[session].lastName = doc.lastName;
                                    rest.sessions[session].role = doc.role;
                                    lib.sendJson(rep, { session: session, user: rest.sessions[session].user, user_id: rest.sessions[session].user_id,
                                        firstName: rest.sessions[session].firstName, lastName: rest.sessions[session].lastName, role: rest.sessions[session].role });
                                    for(var key in rest.sessions) {
                                        if(key != session && rest.sessions[key].ws && rest.sessions[key].ws.readyState == WebSocket.OPEN && rest.sessions[key].role & 2 > 0) {
                                            try {
                                                rest.sessions[key].ws.send(JSON.stringify({action: 'event', text: 'User ' + doc.login + ' logged in'}));
                                            } catch(ex) {
                                                console.log('WebSocket error while sending a message');
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            lib.sendXHRError(rep, 401, 'No username');
                        }
                    }
                    }
                );
                break;
            default:
                lib.sendXHRError(rep, 403, 'Invalid method');
        }
    },

    handle: function (req, rep, session) {

        var role = rest.sessions[session] ? rest.sessions[session].role : 0;

        if(config.restrictedRest && role < 1) {
            lib.sendXHRError(rep, 401, 'not authorized');
            return true;
        }

        var coll = null;
        var order = {};
        var isValid = function(data) { return true; };
        var queryStr = "{}";
        var insertFields = {};
        var preAggregation = [];
        var postAggregation = [];
        var postAction = function() {};

        var a = req.url.match('/[a-zA-Z_]+');
        if(a && Array.isArray(a)) {
            switch(a[0]) {
                case '/persons':
                    coll = rest.persons;
                    order = { lastName: 1, firstName: 1 };
                    queryStr = '{ "$or": [{ "firstName": { "$regex": __search, "$options": "i" }}, { "lastName": { "$regex": __search, "$options": "i" }}] }';
                    isValid = function(data) { return data["firstName"] && data["lastName"] && data.firstName.trim().length > 0 && data.lastName.trim().length > 0; }
                    break;
                case '/groups':
                    coll = rest.groups;
                    order = { name: 1 };
                    queryStr = '{ "name": { "$regex": __search, "$options": "i" }}';
                    isValid = function(data) { return data["name"] && data.name.trim().length > 0; };
                    break;
                case '/messages':
                    coll = rest.messages;
                    order = { time: -1 };
                    queryStr = '{ "group_id": __search }';
                    isValid = function(data) { return data["body"] && data.body.trim().length > 0; };
                    insertFields = { time: Date.now() };
                    postAggregation = [
                        { $lookup: { from: "persons", localField: "author_id", foreignField: "_id", as: "author" } },
                        { $unwind: { path: "$author" } },
                        { $addFields: { authorFirstName: "$author.firstName" } },
                        { $addFields: { authorLastName: "$author.lastName" } },
                        { $project: { author: 0 } }
                    ];
                    postAction = function(newMessage) {
                        for(var key in rest.sessions) {
                            if(key != session && rest.sessions[key].ws && rest.sessions[key].ws.readyState == WebSocket.OPEN && rest.sessions[key].role & 1 > 0) {
                                try {
                                    rest.sessions[key].ws.send(JSON.stringify({action: 'message', group_id: newMessage.group_id }));
                                } catch(ex) {
                                    console.log('WebSocket error while sending a message');
                                }
                            }
                        }
                    };
                    break;
            }
        }
        if(!coll) {
            lib.sendXHRError(rep, 404, 'no such data to operate');
            return true;
        }

        if (req.url == a[0] || req.url.charAt(a[0].length) == '/') {
            var args = req.url.split('/');
            switch (req.method) {

                case 'GET':
                    if(args[2] == '?') {
                        coll.find().count(function(err, n) {
                            if (err) {
                                lib.sendXHRError(rep, 406, 'error in database');
                                return;
                            }
                            lib.sendJson(rep, { count: n } );
                        });
                    } else if(args.length == 3) {
                        // particular object
                        try {
                            var id = ObjectId(args[2]);
                            coll.findOne({_id: id}, function (err, doc) {
                                if (err) {
                                    lib.sendXHRError(rep, 404, 'object not found');
                                    return;
                                }
                                lib.sendJson(rep, doc);
                            });
                        } catch (ex) {
                            lib.sendXHRError(rep, 406, 'not acceptable');
                        }
                    } else if(args.length > 3) {
                        // subset of objects
                        var skip = parseInt(args[2]); if(isNaN(skip)) skip = 0;
                        var limit = parseInt(args[3]); if(isNaN(limit)) limit = 999999;
                        var search = args.length > 4 ? args[4] : '';
                        var query;
                        try {
                            var q = queryStr.replace(/__search/g, '"' + search + '"');
                            query = JSON.parse(q);
                            convertIdstoObjectIds(query);
                        } catch(ex) {
                            query = {};
                        }
                        preAggregation.push({ $match: query });
                        var aggregation = preAggregation.concat(postAggregation);
                        coll.aggregate(aggregation).collation({ locale: 'pl' }).sort(order).skip(skip).limit(limit).toArray(function (err, arr) {
                            if (err) {
                                console.log('ERROR on ' + JSON.stringify(query));
                                lib.sendXHRError(rep, 400, 'Query failed');
                            } else {
                                lib.sendJson(rep, arr);
                            }
                        });
                    } else {
                        // all objects
                        coll.find().toArray(function (err, arr) {
                            if (err) {
                                lib.sendXHRError(rep, 400, 'Query failed');
                            } else {
                                lib.sendJson(rep, arr);
                            }
                        });
                    }
                    break;

                case 'POST':
                    // new object
                    lib.getPayload(req, function(req, err, data) {
                        if (err) {
                            lib.sendXHRError(rep, 400, err);
                        } else {
                            convertIdstoObjectIds(data);
                            if (isValid(data)) {
                                Object.assign(data, insertFields);
                                coll.insertOne(data, function (err, res) {
                                    if (err) {
                                        lib.sendXHRError(rep, 400, 'Insert failed');
                                    } else {
                                        postAction(data);
                                        lib.sendJson(rep, data);
                                    }
                                });
                            } else {
                                lib.sendXHRError(rep, 400, 'Data invalid');
                            }
                        }
                    });
                    break;

                case 'PUT':
                    // update object
                    try {
                        var id = ObjectId(args[2]);
                        lib.getPayload(req, function (req, err, data) {
                            if (err) {
                                lib.sendXHRError(rep, 400, err);
                            } else {
                                delete data._id;
                                convertIdstoObjectIds(data);
                                if (isValid(data)) {
                                    coll.findOneAndUpdate({ _id: id }, { $set: data }, { returnOriginal: false }, function (err, res) {
                                        if (err) {
                                            lib.sendXHRError(rep, 400, 'Update failed');
                                        } else {
                                            lib.sendJson(rep, res.value);
                                        }
                                    });
                                } else {
                                    lib.sendXHRError(rep, 400, 'Data invalid');
                                }
                            }
                        });
                    } catch(ex) {
                        lib.sendXHRError(rep, 406, 'not acceptable');
                    }
                    break;

                case 'DELETE':
                    // delete object
                    try {
                        var id = ObjectId(args[2]);
                        coll.findOne({_id: id}, function(err, doc) {
                            if(err) {
                                lib.sendXHRError(rep, 404, 'Object not found');
                                return;
                            }
                            coll.deleteOne({_id: id}, function (err, data) {
                                if (err) {
                                    lib.sendXHRError(rep, 400, 'Delete failed');
                                } else {
                                    lib.sendJson(rep, doc);
                                }
                            });
                        });
                    } catch (ex) {
                        lib.sendXHRError(rep, 406, 'not acceptable');
                    }
                    break;

                default:
                    lib.sendXHRError(rep, 403, 'Invalid method');
            }
            return true;
        }

        return false;
    }
};