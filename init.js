var fs = require("fs");
var rest = require("./rest");

var feedCollection = function(collection, name, force = false) {
    collection.find().count(function (err, n) {
        if (n == 0 || force) {
            if(n > 0) {
                console.log("Forced initialization of " + name + ", deleting existing data");
                collection.drop();
            } else console.log("No " + name + ", initializing by sample data");
            try {
                var exampleData = JSON.parse(fs.readFileSync(name + ".json", 'utf8'));
                if(exampleData.length > 0) collection.insertMany(exampleData);
            } catch (ex) {
                console.log("Error during " + name + " initialization " + ex);
                process.exit();
            }
        } else {
            console.log(n + " " + name);
        }
    });
}

module.exports = {

    ifNoData: function(force = false) {

        feedCollection(rest.persons, "persons", force);
        feedCollection(rest.groups, "groups", force);
        feedCollection(rest.messages, "messages", force);

    }

};