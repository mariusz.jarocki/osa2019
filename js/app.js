var app = angular.module('app', ['ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ngCookies', 'ngWebSocket']);

app.value('globals', {
    session: '',
    login: '',
    user_id: '',
    firstName: '',
    lastName: '',
    role: 0,
    alert: { text: "" },
    rolesList: { "User": false, "Administrator": false }
});

app.service('common', [ '$uibModal', 'globals', function($uibModal, globals) {

    this.confirm = function(confirmOptions, callback) {

        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title-top',
            ariaDescribedBy: 'modal-body-top',
            templateUrl: '/html/confirm.html',
            controller: 'Confirm',
            controllerAs: 'ctrl',
            resolve: {
                confirmOptions: function () {
                    return confirmOptions;
                }
            }
        });
        modalInstance.result.then(
            function () { callback(true); },
            function (ret) { callback(false); }
        );
    };

    this.showMessage = function(msg) {
        globals.alert.type = 'alert-success';
        globals.alert.text = msg;
    };

    this.showError = function(msg) {
        globals.alert.type = 'alert-danger';
        globals.alert.text = msg;
    };


}]);

app.factory('ws', function($websocket, $rootScope, globals) {

    var dataStream = $websocket('ws://' + window.location.host);

    dataStream.onMessage(function(message) {
        // handling data received from websocket
        try {
            var data = JSON.parse(message.data);
            switch(data.action) {
                case 'event':
                    globals.alert.type = 'alert-danger';
                    globals.alert.text = data.text;
                    break;
                case 'message':
                    $rootScope.$broadcast('newMessage', data.group_id);
                    break;
            }
        } catch(err) {
            console.log('Error during parsing data from websocket: ' + message.data);
        }
    });

    return {

        init: function(session) {
            dataStream.send(JSON.stringify({ action: 'init', session: session }));
        },

    };

});

app.controller("Skeleton", [ '$rootScope', '$window', '$timeout', '$http', '$uibModal', '$cookies', 'globals', 'common', 'ws',
    function($rootScope, $window, $timeout, $http, $uibModal, $cookies, globals, common, ws) {

    var skel = this;
    skel.alert = globals.alert;

    globals.session = $cookies.get('session');

    $http.get('/login').then(
        function(rep) { globals.login = rep.data.user; globals.user_id = rep.data.user_id; 
            globals.firstName = rep.data.firstName; globals.lastName = rep.data.lastName; globals.role = rep.data.role; $rootScope.$broadcast('sessionData'); },
        function(err) { globals.login = ''; globals.user_id = ''; globals.role = 0; $rootScope.$broadcast('sessionData'); }
    );

    skel.closeAlert = function() { skel.alert.text = ""; };

    skel.login = function() {

        if (globals.login) {
            common.confirm({ title: 'Logout', body: 'Are you sure to logout ' + globals.login + '?' }, function(answer) {
                if(answer) {
                    $http.delete('/login').then(
                        function(rep) {
                            common.showMessage(globals.login + ' successfully log out');
                            $timeout(function() { $window.location.href = '/'; }, 2000);
                        },
                        function(err) {
                            common.showMessage(globals.login + ' logout failed');
                        }
                    );
                }
            });
        } else {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: '/html/loginDialog.html',
                controller: 'loginDialog',
                controllerAs: 'ctrl'
            });
            modalInstance.result.then(
                function () {
                    common.showMessage(globals.login + ' successfully log in');
                    $timeout(function() { $window.location.href = '/'; }, 2000);
                },
                function () {}
            );
        }
    };

    ws.init(globals.session);

}]);