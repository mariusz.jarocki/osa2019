app.controller('Persons', ['$http', '$uibModal', 'common', 'globals',
    function($http, $uibModal, common, globals) {
        var ctrl = this;

        ctrl.persons = [];
        ctrl.edited = { firstName: '', lastName: '' };
        ctrl.limit = 1000;
        ctrl.search = '';
        ctrl.selected = 0;
        ctrl.count = 0;

        ctrl.rolesString = function(personRole) {
            var s = "";
            var mask = 1;
            for(var role in globals.rolesList) {
                if((personRole & mask) > 0) {
                    if(s.length > 0) s += ", ";
                    s += role;
                }
                mask <<= 1;
            }
            return s;
        }

        ctrl.refreshData = function() {
            $http.get('/persons/0/' + ctrl.limit + '/' + ctrl.search).then(
                function (response) {
                    ctrl.persons = response.data;
                    ctrl.selected = ctrl.persons.length;
                    $http.get('/persons/?').then(function(response) {
                            ctrl.count = response.data.count;
                    });
                },
                function (err) {
                    common.showError(err.data.error);
                }
            );
        };

        ctrl.refreshData();

        ctrl.insert = function() {
            $http.post("/persons", JSON.stringify(ctrl.edited)).then(
                function(response) {
                    ctrl.persons.push(response.data);
                    ctrl.edited = { firstName: '', lastName: '' };
                },
                function(err) {}
            );
        };

        ctrl.delete = function(n, id) {
            $http.delete("/persons/" + id).then(
                function(response) {
                    ctrl.persons.splice(n, 1);
                    $http.get('/persons/?').then(function(response) {
                        ctrl.count = response.data.count;
                    });
                },
                function(err) {}
            );
        };

        ctrl.editDialog = function(index, id = null) {

            var editData = {index: index, id: id, action: {}};

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: '/html/editPerson.html',
                controller: 'editPerson',
                controllerAs: 'ctrl',
                resolve: {
                    editData: function () {
                        return editData;
                    }
                }
            });

            modalInstance.result.then(
                function () {
                    if (editData.action.err) {
                        common.showError(editData.action.err);
                        return;
                    }
                    switch (editData.action.op) {
                        case 'UPDATE':
                            ctrl.persons[editData.index] = editData.action.result;
                            common.showMessage('Row ' + (editData.index + 1) + ' updated');
                            break;
                        case 'INSERT':
                            ctrl.refreshData();
                            common.showMessage('Row added');
                            break;
                        case 'DELETE':
                            ctrl.persons.splice(editData.index, 1);
                            common.showMessage('Row deleted');
                            break;
                    }
                },
                function (ret) {
                }
            );
        };

}]);