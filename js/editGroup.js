app.controller("editGroup", [ '$http', '$uibModalInstance', 'editData', 'common', function($http, $uibModalInstance, editData, common) {

    var ctrl = this;
    ctrl.editData = editData;
    ctrl.edited = { name: '' };

    if(ctrl.editData.id) {
        $http.get('/groups/' + editData.id).then(
            function (rep) {
                ctrl.edited = rep.data;
            },
            function (err) {

            });
    }

    ctrl.ok = function () {
        if(ctrl.editData.id) {
            ctrl.editData.action.op = 'UPDATE';
            $http.put('/groups/' + editData.id, ctrl.edited).then(
                function(rep) {
                    ctrl.editData.action.result = rep.data;
                    $uibModalInstance.close();
                },
                function(err) {
                    ctrl.editData.action.err = err;
                    $uibModalInstance.close();
                }
            );
        } else {
            ctrl.editData.action.op = 'INSERT';
            $http.post('/groups', ctrl.edited).then(
                function(rep) {
                    ctrl.editData.action.result = rep.data;
                    $uibModalInstance.close();
                },
                function(err) {
                    ctrl.editData.action.err = err;
                    $uibModalInstance.close();
                }
            );
        }
    };

    ctrl.delete = function () {
        if(ctrl.editData.id) {
            $http.get('/groups/' + ctrl.editData.id).then(
                function(response) {
                    common.confirm({
                        title: 'Confirm',
                        body: 'Are you going to delete the group <strong>' + response.data.name + '</strong>?'
                    }, function (answer) {

                        if(!answer) {
                            $uibModalInstance.dismiss('cancel');
                            return;
                        }

                        ctrl.editData.action.op = 'DELETE';
                        $http.delete('/groups/' + editData.id).then(
                            function (rep) {
                                ctrl.editData.action.result = rep.data;
                                $uibModalInstance.close();
                            },
                            function (err) {
                                ctrl.editData.action.err = err;
                                $uibModalInstance.close();
                            }
                        );

                    });

                },
                function(err) {
                    ctrl.editData.action.err = err;
                    $uibModalInstance.close();
                }
            );
        }
    };

    ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);