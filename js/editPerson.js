app.controller("editPerson", [ '$http', '$uibModalInstance', 'editData', 'common', 'globals', function($http, $uibModalInstance, editData, common, globals) {

    var ctrl = this;
    ctrl.editData = editData;
    ctrl.edited = { firstName: '', lastName: '', role: 0 };
    ctrl.rolesList = globals.rolesList;

    if(ctrl.editData.id) {
        $http.get('/persons/' + editData.id).then(
            function (rep) {
                ctrl.edited = rep.data;
                var mask = 1;
                for(var role in ctrl.rolesList) {
                    ctrl.rolesList[role] = (ctrl.edited.role & mask) > 0;
                    mask <<= 1;
                }
            },
            function (err) {}
        );
    }

    ctrl.ok = function () {
        ctrl.edited.role = 0;
        var mask = 1;
        for(var role in ctrl.rolesList) {
            if(ctrl.rolesList[role]) ctrl.edited.role |= mask;
            mask <<= 1;
        }
        if(ctrl.editData.id) {
            ctrl.editData.action.op = 'UPDATE';
            $http.put('/persons/' + editData.id, ctrl.edited).then(
                function(rep) {
                    ctrl.editData.action.result = rep.data;
                    $uibModalInstance.close();
                },
                function(err) {
                    ctrl.editData.action.err = err;
                    $uibModalInstance.close();
                }
            );
        } else {
            ctrl.editData.action.op = 'INSERT';
            $http.post('/persons', ctrl.edited).then(
                function(rep) {
                    ctrl.editData.action.result = rep.data;
                    $uibModalInstance.close();
                },
                function(err) {
                    ctrl.editData.action.err = err;
                    $uibModalInstance.close();
                }
            );
        }
    };

    ctrl.delete = function () {
        if(ctrl.editData.id) {
            $http.get('/persons/' + ctrl.editData.id).then(
                function(response) {
                    common.confirm({
                        title: 'Confirm',
                        body: 'Are you going to delete <strong>' + response.data.firstName + ' ' + response.data.lastName + '</strong>?'
                    }, function (answer) {

                        if(!answer) {
                            $uibModalInstance.dismiss('cancel');
                            return;
                        }

                        ctrl.editData.action.op = 'DELETE';
                        $http.delete('/persons/' + editData.id).then(
                            function (rep) {
                                ctrl.editData.action.result = rep.data;
                                $uibModalInstance.close();
                            },
                            function (err) {
                                ctrl.editData.action.err = err;
                                $uibModalInstance.close();
                            }
                        );

                    });

                },
                function(err) {
                    ctrl.editData.action.err = err;
                    $uibModalInstance.close();
                }
            );
        }
    };

    ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);