app.constant('routes', [
	{ route: '/', templateUrl: '/html/default.html', controller: 'Default', controllerAs: 'ctrl', menu: '<i class="fa fa-lg fa-home"></i>' },
	{ route: '/persons', templateUrl: '/html/persons.html', controller: 'Persons', controllerAs: 'ctrl', menu: 'Persons', roleMask: 2 },
	{ route: '/groups', templateUrl: '/html/groups.html', controller: 'Groups', controllerAs: 'ctrl', menu: 'Groups', roleMask: 3 },
	{ route: '/messages', templateUrl: '/html/messages.html', controller: 'Messages', controllerAs: 'ctrl', menu: 'Messages', roleMask: 1 }
]);

app.config(['$routeProvider', '$locationProvider', 'routes', function($routeProvider, $locationProvider, routes) {
    $locationProvider.hashPrefix('');
	for(var i in routes) {
		$routeProvider.when(routes[i].route, routes[i]);
	}
	$routeProvider.otherwise({ redirectTo: '/' });
}]);

app.controller('Menu', ['$scope', '$location', 'routes', 'globals',
	function($scope, $location, routes, globals) {
		var ctrl = this;

		ctrl.menu = [];

		$scope.$on('sessionData', function () {
			for (var i in routes) {
				if (routes[i].menu && (!routes[i].roleMask || routes[i].roleMask & globals.role)) {
					ctrl.menu.push({route: routes[i].route, title: routes[i].menu});
				}
			}
		});

        ctrl.isCollapsed = true;

        $scope.$on('$routeChangeSuccess', function () {
            ctrl.isCollapsed = true;
        });

		ctrl.navClass = function(page) {
			return page === $location.path() ? 'active' : '';
		}

		ctrl.loginIcon = function() {
			return globals.login ? globals.firstName + ' ' + globals.lastName + '&nbsp;<span class="fa fa-lg fa-sign-out"></span>' : '<span class="fa fa-lg fa-sign-in"></span>';
		}
	}
]);