app.controller('Messages', ['$http', '$scope', 'common', 'globals',
    function($http, $scope, common, globals) {
        var ctrl = this;

        ctrl.messages = [];
        ctrl.limit = 1000;
        ctrl.search = '';
        ctrl.selected = 0;
        ctrl.body = '';
        ctrl.group_id = '';
        ctrl.groups = [];

        ctrl.refreshData = function() {
            $http.get('/messages/0/' + ctrl.limit + '/' + ctrl.group_id).then(
                function (response) {
                    ctrl.messages = response.data;
                    ctrl.selected = ctrl.messages.length;
                },
                function (err) {
                    common.showError(err.data.error);
                }
            );
        };

        $scope.$on('newMessage', function(event, arg) {
            if(arg == ctrl.group_id)
                ctrl.refreshData();
        });

        $http.get('/groups/0//').then(
            function(rep) {
                ctrl.groups = rep.data.filter(function(group) { return group["member_ids"] && group.member_ids.includes(globals.user_id); });
                if(ctrl.groups.length > 0) ctrl.group_id = ctrl.groups[0]._id;
                ctrl.refreshData();
            },
            function(err) {
                common.showError(err.data.error);
            }
        );

        ctrl.sendMessage = function() {
            var message = { group_id: ctrl.group_id, author_id: globals.user_id, body: ctrl.body };
            $http.post('/messages', message).then(
                function(rep) {
                    rep.data.authorFirstName = globals.firstName;
                    rep.data.authorLastName = globals.lastName;
                    ctrl.messages.unshift(rep.data);
                },
                function(err) {}
            );
            ctrl.body = '';
        };

        ctrl.toDateStr = function(stamp) {
            return new Date(stamp).toLocaleString("pl-PL");
        }
}]);