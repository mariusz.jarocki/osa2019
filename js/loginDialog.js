app.controller("loginDialog", [ '$http', '$uibModalInstance', 'globals', 'common', function($http, $uibModalInstance, globals, common) {
    var ctrl = this;

    ctrl.edited = { username: '', password: '' };

    ctrl.login = function() {

        $http.post('/login', ctrl.edited).then(
            function(rep) {
                globals.login = rep.data.user;
                globals.firstName = rep.data.firstName;
                globals.lastName = rep.data.lastName;
                $uibModalInstance.close();
            },
            function(err) { common.showError('Login failed'); }
        );
    };

    ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);