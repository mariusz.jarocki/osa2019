app.controller('Groups', ['$http', '$uibModal', 'common', 'globals',
    function($http, $uibModal, common, globals) {
        var ctrl = this;

        ctrl.groups = [];
        ctrl.edited = { name: '' };
        ctrl.limit = 1000;
        ctrl.search = '';
        ctrl.selected = 0;
        ctrl.count = 0;

        ctrl.isAdmin = function() { return (globals.role & 2) > 0; }
        ctrl.isUser = function() { return (globals.role & 1) > 0; }

        ctrl.refreshData = function() {
            $http.get('/groups/0/' + ctrl.limit + '/' + ctrl.search).then(
                function (response) {
                    ctrl.groups = response.data;
                    ctrl.groups.forEach(function(group) {
                        group.amIMember = group.member_ids && group.member_ids.includes(globals.user_id);
                    });
                    ctrl.selected = ctrl.groups.length;
                    $http.get('/groups/?').then(function(response) {
                            ctrl.count = response.data.count;
                    });
                },
                function (err) {
                    common.showError(err.data.error);
                }
            );
        };

        ctrl.refreshData();

        ctrl.insert = function() {
            $http.post("/groups", JSON.stringify(ctrl.edited)).then(
                function(response) {
                    ctrl.groups.push(response.data);
                    ctrl.edited = { name: '' };
                },
                function(err) {}
            );
        };

        ctrl.delete = function(n, id) {
            $http.delete("/groups/" + id).then(
                function(response) {
                    ctrl.groups.splice(n, 1);
                    $http.get('/groups/?').then(function(response) {
                        ctrl.count = response.data.count;
                    });
                },
                function(err) {}
            );
        };

        ctrl.changeMembership = function(group) {
            if(!group.member_ids) group.member_ids = [];
            if(group.amIMember) {
                group.member_ids.push(globals.user_id);
            } else {
                group.member_ids = group.member_ids.filter(function(m) { return m != globals.user_id; });
            }
            var amIMember = group.amIMember;
            delete group.amIMember;
            $http.put('/groups/' + group._id, group).then(
                function(rep) {},
                function(err) {}
            );
            group.amIMember = amIMember;
        };

        ctrl.editDialog = function(index, id = null) {

            if(!ctrl.isAdmin()) return;

            var editData = {index: index, id: id, action: {}};

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: '/html/editGroup.html',
                controller: 'editGroup',
                controllerAs: 'ctrl',
                resolve: {
                    editData: function () {
                        return editData;
                    }
                }
            });

            modalInstance.result.then(
                function () {
                    if (editData.action.err) {
                        common.showError(editData.action.err);
                        return;
                    }
                    switch (editData.action.op) {
                        case 'UPDATE':
                            ctrl.groups[editData.index] = editData.action.result;
                            common.showMessage('Row ' + (editData.index + 1) + ' updated');
                            break;
                        case 'INSERT':
                            ctrl.refreshData();
                            common.showMessage('Row added');
                            break;
                        case 'DELETE':
                            ctrl.groups.splice(editData.index, 1);
                            common.showMessage('Row deleted');
                            break;
                    }
                },
                function (ret) {
                }
            );
        };

}]);