var fs = require('fs');
var url = require('url');
var path = require('path');
var mime = require('mime');

var lib = module.exports = {

    sendErrorOnStaticContent: function (response, code) {
        response.writeHead(code, {'Content-Type': 'text/plain; charset=utf-8'});
        switch (code) {
            case 404:
                response.write('Error 404: file not found.');
                break;
            case 403:
                response.write('Error 403: access denied.');
                break;
            case 406:
                response.write('Error 406: not acceptable');
                break;
            default:
                response.write('Error ' + code);
        }
        response.end();
    },

    sendFile: function (response, filePath, fileContents) {
        response.writeHead(200, {'Content-Type': mime.getType(path.basename(filePath))});
        response.end(fileContents);
    },

    serveStaticContent: function (response, absPath) {
        var n = absPath.indexOf('?');
        var fileName = absPath.substring(0, n != -1 ? n : absPath.length);
        fs.exists(fileName, function (exists) {
            if (exists) {
                fs.readFile(fileName, function (err, data) {
                    if (err) {
                        lib.sendErrorOnStaticContent(response, 406);
                    } else {
                        lib.sendFile(response, fileName, data);
                    }
                });
            } else {
                lib.sendErrorOnStaticContent(response, 404);
            }
        });
    },

    sendJson: function (response, obj) {
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end(JSON.stringify(obj));
    },

    sendXHRError: function (response, code, msg) {
        response.writeHead(code, {'Content-Type': 'application/json'});
        response.end(JSON.stringify({error: msg}));
    },

    getPayload: function(request, callback) {
        request.setEncoding('utf8');
        var payload = '';

        request.on('data', function (data) {
            payload += data;
        });

        request.on('end', function () {
            try {
                callback(request, '', JSON.parse(payload));
            } catch (ex) {
                callback(request, 'Invalid JSON', null);
            }
        });
    }
};