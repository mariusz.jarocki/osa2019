var http = require('http');
var fs = require('fs');

var intl = require('intl');
var mongodb = require('mongodb');
var cookies = require('cookies');
var uuid = require('uuid');
var WebSocket = require('ws');

var config = require('./config.js');
var lib = require('./lib');
var rest = require('./rest');
var init = require('./init');

var mongo = mongodb.MongoClient;
var ObjectId = mongodb.ObjectId;

var httpServer = http.createServer();

httpServer.on('request', function (req, rep) {

    var appCookies = new cookies(req, rep);
    var session = appCookies.get('session');
    var now = Date.now();
    if(!session || !rest.sessions[session]) {
        session = uuid();
        rest.sessions[session] = { from: req.connection.remoteAddress, created: now, touched: now, user: null, role: 0 };
        appCookies.set('session', session, { httpOnly: false });
    } else {
        rest.sessions[session].touched = now;
        appCookies.set('session', session, { httpOnly: false });
    }

    console.log(new intl.DateTimeFormat("pl-PL", { month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' }).format(new Date(now)) + ' ' + req.connection.remoteAddress + ' ' + session + ' ' + req.method + ' ' + req.url);

	if(req.url == '/') {
		lib.serveStaticContent(rep, 'html/index.html');
	} else if(req.url == '/favicon.ico') {
		lib.serveStaticContent(rep, 'img/favicon.ico');
	} else if(req.url == '/login') {
	    // handled by rest module
	    rest.login(req, rep, session);
    } else if(/^\/(html|css|js|fonts|img)\//.test(req.url)) {
        lib.serveStaticContent(rep, '.' + req.url);
    } else {
	    if(!rest.handle(req, rep, session)) {
	        lib.sendErrorOnStaticContent(rep, 403);
        }
    }
});

var ws = new WebSocket.Server({ server: httpServer });

ws.on('connection', function connection(conn) {

    conn.on('message', function(message) {
    // handling websocket messages from clients

        var now = Date.now();
        console.log(new intl.DateTimeFormat("pl-PL", { month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' }).format(new Date(now)) + ' WSMSG ' + message);

        var rep;
        try {
            msg = JSON.parse(message);
            rep = { action: msg.action };
            switch(msg.action) {
                case 'init':
                    if(rest.sessions[msg.session]) {
                        rest.sessions[msg.session].ws = conn;
                        conn.session = msg.session;
                        rep.session = msg.session;
                    } else {
                        rep.error = 'Session ' + msg.session + ' invalid';
                    }
                    break;
                default:
                    rep.error = 'No valid action in message';
            }
        } catch(ex) {
            rep.error = 'Invalid message from websocket';
        }
        try {
            conn.send(JSON.stringify(rep));
        } catch(ex) {
            console.err('WebSocket error while sending initialization response');
        }
    });

});

mongo.connect(config.dbUrl, { useNewUrlParser: true }, function(err, conn) {

    if(err) {
        console.log("Connection to database failed");
        process.exit();
    }

    console.log("Connection to database established");

    var db = conn.db(config.dbName);
    rest.persons = db.collection("persons");
    rest.groups = db.collection("groups");
    rest.messages = db.collection("messages");
    init.ifNoData(config.forceDataInit);

    try {
        httpServer.listen(config.listeningPort);
    } catch(ex) {
        console.log("Port " + config.listeningPort + " cannot be used");
        process.exit();
    }
    console.log("HTTP server is listening on the port " + config.listeningPort);
});