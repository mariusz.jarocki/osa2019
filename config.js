var config = module.exports = {
    dbUrl: "mongodb://localhost",
    dbName: "osa2019",
    forceDataInit: false,
    listeningPort: 8888,
    restrictedRest: false 
};